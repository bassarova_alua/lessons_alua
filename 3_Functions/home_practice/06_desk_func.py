# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 600)


# Создать функцию которая нарисует подобие шахматной доски по двум параметрам.

# Функцию будет следующего вида   def desk(y, x):
# Где у = высота доски(количество квадратов по вертикали), х = ширина доски(количесво квадратов по горизонтали)


def desk(y, x):
    y = y * 101
    x = x * 101
    for step in range(100, 201, 100):
        for y_1 in range(step, y, 200):
            for x_1 in range(step, x, 200):
                left_bottom = sd.get_point(x_1, y_1)
                sd.square(left_bottom=left_bottom, side=100, color=sd.COLOR_BLACK)
        for y_1 in range(100, y, 200):
            for x_1 in range(200, x, 200):
                left_bottom = sd.get_point(x_1, y_1)
                sd.square(left_bottom=left_bottom, side=100, color=sd.COLOR_WHITE)
        for y_1 in range(200, y, 200):
            for x_1 in range(100, x, 200):
                left_bottom = sd.get_point(x_1, y_1)
                sd.square(left_bottom=left_bottom, side=100, color=sd.COLOR_WHITE)


desk(4, 10)
sd.pause()
