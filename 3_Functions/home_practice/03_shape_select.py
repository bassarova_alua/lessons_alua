import simple_draw as sd

sd.resolution = (800, 600)


# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана
#
# Код функций из упр 02_global_color.py скопировать сюда
# Результат решения см results/exercise_03_shape_select.jpg
def figures_0(point, angle, length, iteration, step_a):
    for _ in range(iteration):
        triangle = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
        triangle.draw()
        point = triangle.end_point
        angle += step_a


# TODO здесь ваш код
def figures(point, angle, length):
    while True:
        user_input = input('Возможные фигуры: '
                           '\n0 : треугольник'
                           '\n1 : квадрат'
                           '\n2 : пятиугольнмк'
                           '\n3 : шестиугольник'
                           '\n'
                           'Введите желаемую фигуру > ')
        some_dict = \
            {'0': [3, 120],
             '1': [4, 90],
             '2': [5, 72],
             '3': [6, 60]}
        if user_input in some_dict:
            figures_0(point, angle, length, iteration=some_dict[user_input][0], step_a=some_dict[user_input][1])
            break
        else:
            print('Я просил цифру от 0 до 3, а вы ввели', user_input + ",", 'попробуйте еще раз!')


point_0 = sd.get_point(400, 300)
figures(point=point_0, angle=20, length=100)
sd.pause()

