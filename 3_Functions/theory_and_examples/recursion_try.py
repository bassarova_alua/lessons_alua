# goods = {
#     'Лампа': '12345',
#     'Стол': '23456',
#     'Диван': '34567',
#     'Стул': '45678',
# }
# store = {
#     '12345': [
#         {'quantity': 27, 'price': 42},
#     ],
#     '23456': [
#         {'quantity': 22, 'price': 510},
#         {'quantity': 32, 'price': 520},
#     ],
#     '34567': [
#         {'quantity': 2, 'price': 1200},
#         {'quantity': 1, 'price': 1150},
#     ],
#     '45678': [
#         {'quantity': 50, 'price': 100},
#         {'quantity': 12, 'price': 95},
#         {'quantity': 43, 'price': 97},
#     ],
# }
Techno = {
     'industrial': {
         'Monasterio 7 years': {
             'artist0': 'Gemut',
              'artist1': 'Ansome live',
              'artist2': 'Perc',
              'artist3': 'Stef_Mendesidis',
              'artist4': 'Relict-1',
              'artist5': 'Roks',
              'artist6': 'Unbalance',
              'artist7': 'Yaroslav_Lebedinskiy',
       },
        'Monasterio season 2021': {
             'art': 'Coaxial veins',
             'art1': 'Gemut',
             'art2': 'Kyk',
             'art3': 'Mrac',
             'art4': 'Relict-1',
             'art5': 'Roks',
             'art6': 'Syberian',
             'art7': 'Sub imperium',
             'art9': 'Trast True',
             'art10': 'UH',
             'art11': 'Unbalance',
             'art12': 'Wacky Kid',
         },
         'Monasterio rave': {
             'art13': 'Dax J',
             'art14': 'Manni Dee',
             'art15': 'Oscar Mulero',
             'art16': 'Perc',
         },
         'Monasterio night': {
             'art17': 'Alod',
             'art18': 'Chain Damage',
             'art19': 'Draag',
             'art20': 'Gutkein',
             'art21': 'Inox Traxx',
             'art22': 'Okkultative',
             'art23': 'Serotonin crisis',
             'art24': 'Yaroslav Lebedinskiy',
         }
     }
 }
def find_element(tree, element_name):
    if element_name in tree:
        return tree[element_name]
    for key, sub_tree in tree.items():
        if isinstance(sub_tree, dict):
            result = find_element(tree=sub_tree, element_name=element_name)
            if result:
                break
    else:
        result = None
    return result
#
#
res = find_element(tree=Techno, element_name='art5')
#
print(res)
#
