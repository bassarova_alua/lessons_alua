# -*- coding: utf-8 -*-
import simple_draw as sd

sd.background_color = sd.COLOR_BLACK
sd.resolution = (800, 600)


# Добавить цвет в функции рисования геом. фигур. из упр 01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см results/exercise_02_global_color.jpg

# TODO здесь ваш код
def figures(point, angle, length, iteration, step_a):
    while True:
        user_input = input('Возможные цвета:'
                           '\n0 : red'
                           '\n1 : orange'
                           '\n2 : yellow'
                           '\n3 : green'
                           '\n4 : cyan'
                           '\n5 : blue'
                           '\n6 : purple'
                           '\n'
                           'Введите желаемый цвет:')
        some_dict = \
            {'0': sd.COLOR_RED,
             '1': sd.COLOR_ORANGE,
             '2': sd.COLOR_YELLOW,
             '3': sd.COLOR_GREEN,
             '4': sd.COLOR_CYAN,
             '5': sd.COLOR_BLUE,
             '6': sd.COLOR_PURPLE}
        if user_input in some_dict:
            color = some_dict[user_input]
            for a in range(iteration):
                triangle = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
                triangle.draw(color=color)
                point = triangle.end_point
                angle += step_a
            break
        else:
            print('Я просил цифру от 0 до 6, а вы ввели', user_input+",", 'попробуйте еще раз!')


point_0 = sd.get_point(100, 100)
point_1 = sd.get_point(600, 100)
point_2 = sd.get_point(100, 350)
point_3 = sd.get_point(600, 350)

figures(point=point_0, angle=20, length=100, iteration=3, step_a=120)
figures(point=point_1, angle=20, length=100, iteration=4, step_a=90)
figures(point=point_2, angle=20, length=100, iteration=5, step_a=72)
figures(point=point_3, angle=20, length=100, iteration=6, step_a=60)
sd.pause()
