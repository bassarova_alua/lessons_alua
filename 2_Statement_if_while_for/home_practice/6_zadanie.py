# (цикл for)
import simple_draw as sd



# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for


# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич

