# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 800)


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,
# TODO Мой код-1:
# def draw_branches(point, angle, length, delta):
#     vector_0 = sd.get_vector(start_point=point, angle=angle, length=length)
#     vector_0.draw()
#     vector_1 = sd.get_vector(start_point=vector_0.end_point, angle=angle + delta, length=length)
#     vector_1.draw()
#     vector_2 = sd.get_vector(start_point=vector_0.end_point, angle=angle - delta, length=length)
#     vector_2.draw()
#
#
# point_0 = sd.get_point(400, 0)
# draw_branches(point=point_0, angle=90, length=100, delta=30)
# sd.pause()
# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 10 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длинна ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

# 3) Запустить вашу рекурсивную функцию, используя следующие параметры:
# root_point = sd.get_point(300, 30)
# draw_branches(start_point=root_point, angle=90, length=100)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения
# TODO Мой код-2:
# def draw_branches(point, angle, length, delta):
#     if length < 1:
#         return
#     vector_0 = sd.get_vector(start_point=point, angle=angle, length=length)
#     vector_0.draw()
#     next_point = vector_0.end_point
#     next_angle = angle - delta
#     next_angle_0 = angle + delta
#     next_length = length * .75
#     draw_branches(point=next_point, angle=next_angle, length=next_length, delta=delta)
#     draw_branches(point=next_point, angle=next_angle_0, length=next_length, delta=delta)
#
#
# root_point = sd.get_point(300, 30)
# draw_branches(point=root_point, angle=90, length=100, delta=30)
# sd.pause()
# 4) Усложненное задание (делать поосле зачета первой части)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см results/exercise_04_fractal_02.jpg

# Пригодятся функции
# sd.random_number()
# TODO Мой код-3:
def draw_branches(point, angle, length, color):
    if length < 5:
        return
    if length < 10:
        color = sd.COLOR_GREEN
    vector_0 = sd.get_vector(start_point=point, angle=angle, length=length)
    vector_0.draw(color=color)
    next_point = vector_0.end_point
    next_angle = angle - sd.random_number(a=18, b=42)
    next_angle_0 = angle + sd.random_number(a=18, b=42)
    next_length = length * sd.random_number(a=60, b=90) / 100
    draw_branches(point=next_point, angle=next_angle, length=next_length, color=color)
    draw_branches(point=next_point, angle=next_angle_0, length=next_length, color=color)


root_point = sd.get_point(300, 30)
draw_branches(point=root_point, angle=90, length=100, color=sd.COLOR_YELLOW)
sd.pause()
