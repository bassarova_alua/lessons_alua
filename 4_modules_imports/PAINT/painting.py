# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 800)


def rainbow():
    rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                      sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)
    point = sd.get_point(1600, 0)
    radius = 775
    for a in rainbow_colors:
        sd.circle(center_position=point, radius=radius, color=a, width=10)
        radius += 10


rainbow()


def soil():
    left_bottom = sd.get_point(0, 0)
    right_top = sd.get_point(1200, 125)
    sd.rectangle(left_bottom=left_bottom, right_top=right_top, color=sd.COLOR_DARK_GREEN, width=0)


soil()


def house():
    y = 125
    y1 = 145
    y2 = 145
    y3 = 165
    e = 0
    u = 0
    left_bottom_point = sd.get_point(300, y)
    right_top_point = sd.get_point(340, y2)
    color = sd.COLOR_YELLOW
    sd.rectangle(left_bottom=sd.get_point(300, 125), right_top=sd.get_point(560, 325), color=sd.COLOR_YELLOW, width=3)
    for q in range(1, 13):
        if q % 2 == 0:
            for x in range(0, 201, 40):
                sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, color=color, width=3)
                left_bottom_point = sd.get_point(300 + x, y + e)
                right_top_point = sd.get_point(340 + x, y2 + e)
            e += 40
        else:
            for x in range(0, 201, 40):
                sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, color=color, width=3)
                left_bottom_point = sd.get_point(320 + x, y1 + u)
                right_top_point = sd.get_point(360 + x, y3 + u)
            u += 40
    sd.rectangle(left_bottom=sd.get_point(300, 185), right_top=sd.get_point(400, 285), color=sd.COLOR_DARK_YELLOW,
                 width=0)
    sd.rectangle(left_bottom=sd.get_point(460, 185), right_top=sd.get_point(560, 285), color=sd.COLOR_DARK_YELLOW,
                 width=0)


house()


def smile(a, b):
    color = sd.COLOR_GREEN
    radius = 37
    point = sd.get_point(a, b)
    sd.circle(center_position=point, radius=radius, color=color, width=0)
    color2 = sd.COLOR_RED
    radius2 = 5
    point2 = sd.get_point(a + 12, b + 12)
    sd.circle(center_position=point2, radius=radius2, color=color2, width=0)
    color3 = sd.COLOR_RED
    radius3 = 5
    point3 = sd.get_point(a - 12, b + 12)
    sd.circle(center_position=point3, radius=radius3, color=color3, width=0)
    color4 = sd.COLOR_GREEN
    radius4 = 10
    point4 = sd.get_point(a - 32, b + 7)
    sd.circle(center_position=point4, radius=radius4, color=color4, width=0)
    color5 = sd.COLOR_GREEN
    radius5 = 10
    point5 = sd.get_point(a + 32, b + 7)
    sd.circle(center_position=point5, radius=radius5, color=color5, width=0)
    color6 = sd.COLOR_DARK_BLUE
    radius6 = 2
    point6 = sd.get_point(a - 37, b + 2)
    sd.circle(center_position=point6, radius=radius6, color=color6, width=2)
    color7 = sd.COLOR_DARK_BLUE
    radius7 = 2
    point7 = sd.get_point(a + 37, b + 2)
    sd.circle(center_position=point7, radius=radius7, color=color7, width=2)
    left_bottom = sd.get_point(a - 3, b - 10)
    right_top = sd.get_point(a + 3, b + 7)
    color = sd.COLOR_ORANGE
    sd.rectangle(left_bottom=left_bottom, right_top=right_top, color=color, width=0)
    left_bottom2 = sd.get_point(a - 17, b - 17)
    right_top2 = sd.get_point(a + 17, b - 12)
    color2 = sd.COLOR_ORANGE
    sd.rectangle(left_bottom=left_bottom2, right_top=right_top2, color=color2, width=0)


smile(345, 235)
smile(510, 235)


def roof():
    points = [sd.get_point(250, 325), sd.get_point(600, 325), sd.get_point(425, 500)]
    sd.polygon(point_list=points, color=sd.COLOR_RED, width=0)


roof()


def draw_branches(point, angle, length, color):
    if length < 5:
        return
    if length < 10:
        color = sd.COLOR_GREEN
    vector_0 = sd.get_vector(start_point=point, angle=angle, length=length)
    vector_0.draw(color=color)
    next_point = vector_0.end_point
    next_angle = angle - sd.random_number(a=18, b=42)
    next_angle_0 = angle + sd.random_number(a=18, b=42)
    next_length = length * sd.random_number(a=60, b=90) / 100
    draw_branches(point=next_point, angle=next_angle, length=next_length, color=color)
    draw_branches(point=next_point, angle=next_angle_0, length=next_length, color=color)


root_point = sd.get_point(1000, 125)
draw_branches(point=root_point, angle=90, length=80, color=sd.COLOR_YELLOW)


def sun(a, b, iteration, angle, length, step_a):
    color = sd.COLOR_YELLOW
    point = sd.get_point(a, b)
    sd.circle(center_position=point, radius=50, color=color, width=0)
    for _ in range(iteration):
        rays = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
        rays.draw()
        angle += step_a


sun(a=650, b=625, iteration=15, angle=0, length=100, step_a=24)


def snowfall():
    x_cor = []
    for x in range(50, 51 * 3, 25):
        x_cor.append(x)
    y_cor = []
    for _ in range(20):
        while True:
            y = sd.random_number(a=700, b=800)
            if y in y_cor:
                y = sd.random_number(a=700, b=800)
            else:
                break
        y_cor.append(y)
    length_0 = []
    for _ in range(20):
        while True:
            len_a = sd.random_number(a=10, b=30)
            if len_a in length_0:
                len_a = sd.random_number(a=10, b=30)
            else:
                break
        length_0.append(len_a)
    while True:
        sd.start_drawing()
        for index, x in enumerate(x_cor):
            y = y_cor[index]
            length = length_0[index]
            snow_point = sd.get_point(x, y)
            sd.snowflake(center=snow_point, length=length, color=sd.background_color)
            y_cor[index] -= sd.random_number(a=0, b=50)
            x_cor[index] -= sd.random_number(a=-10, b=+10)
            snow_point = sd.get_point(x_cor[index], y_cor[index])
            sd.snowflake(center=snow_point, length=length, color=sd.COLOR_WHITE)
            if y_cor[index] < 125:
                p_point = sd.get_point(x_cor[index], y_cor[index])
                sd.snowflake(center=p_point, length=length, color=sd.COLOR_WHITE)
                y_cor[index] += 700
        sd.finish_drawing()
        sd.sleep(0.1)
        if sd.user_want_exit():
            break


snowfall()

sd.pause()
