# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (800, 600)


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Примерный алгоритм внутри функции:
#   # будем рисовать с помощью векторов, каждый следующий - из конечной точки предыдущего
#   текущая_точка = начальная точка
#   для угол_наклона из диапазона от 0 до 360 с шагом XXX
#      # XXX подбирается индивидуально для каждой фигуры
#      составляем вектор из текущая_точка заданной длины с наклоном в угол_наклона
#      рисуем вектор
#      текущая_точка = конечной точке вектора
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# sd.line()
# Результат решения см /results/exercise_01_shapes.jpg

# TODO здесь ваш код
# def triangle(point, angle, length):
#     Triangle = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
#     Triangle.draw()
#     Triangle2 = sd.get_vector(start_point=Triangle.end_point, angle=angle + 120, length=length, width=5)
#     Triangle2.draw()
#     Triangle3 = sd.get_vector(start_point=Triangle2.end_point, angle=angle + 240, length=length, width=5)
#     Triangle3.draw()
#
#
# def square(point, angle, length):
#     Square = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
#     Square.draw()
#     Square2 = sd.get_vector(start_point=Square.end_point, angle=angle + 90, length=length, width=5)
#     Square2.draw()
#     Square3 = sd.get_vector(start_point=Square2.end_point, angle=angle + 180, length=length, width=5)
#     Square3.draw()
#     Square4 = sd.get_vector(start_point=Square3.end_point, angle=angle + 270, length=length, width=5)
#     Square4.draw()
#
#
# def pentagon(point, angle, length):
#     Pentagon = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
#     Pentagon.draw()
#     Pentagon2 = sd.get_vector(start_point=Pentagon.end_point, angle=angle + 72, length=length, width=5)
#     Pentagon2.draw()
#     Pentagon3 = sd.get_vector(start_point=Pentagon2.end_point, angle=angle + 144, length=length, width=5)
#     Pentagon3.draw()
#     Pentagon4 = sd.get_vector(start_point=Pentagon3.end_point, angle=angle + 216, length=length, width=5)
#     Pentagon4.draw()
#     Pentagon5 = sd.get_vector(start_point=Pentagon4.end_point, angle=angle + 288, length=length, width=5)
#     Pentagon5.draw()
#
#
# def hexagon(point, angle, length):
#     Hexagon = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
#     Hexagon.draw()
#     Hexagon2 = sd.get_vector(start_point=Hexagon.end_point, angle=angle + 60, length=length, width=5)
#     Hexagon2.draw()
#     Hexagon3 = sd.get_vector(start_point=Hexagon2.end_point, angle=angle + 120, length=length, width=5)
#     Hexagon3.draw()
#     Hexagon4 = sd.get_vector(start_point=Hexagon3.end_point, angle=angle + 180, length=length, width=5)
#     Hexagon4.draw()
#     Hexagon5 = sd.get_vector(start_point=Hexagon4.end_point, angle=angle + 240, length=length, width=5)
#     Hexagon5.draw()
#     Hexagon6 = sd.get_vector(start_point=Hexagon5.end_point, angle=angle + 300, length=length, width=5)
#     Hexagon6.draw()
#
#
# # TODO функций должно быть четыре, тоесть на каждую фигуру должна быть своя функция!
# point_0 = sd.get_point(100, 100)
# point_1 = sd.get_point(600, 100)
# point_2 = sd.get_point(100, 350)
# point_3 = sd.get_point(600, 350)
# triangle(point=point_0, angle=20, length=100)
# square(point=point_1, angle=20, length=100)
# pentagon(point=point_2, angle=20, length=100)
# hexagon(point=point_3, angle=20, length=100)
# sd.pause()

# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44? Код писать не нужно, просто представь объем работы... и запомни это.


# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв в начальной/конечной точках рисуемой фигуры
# (если он есть. подсказка - на последней итерации можно использовать линию от первой точки)

# TODO здесь ваш код
def figures(point, angle, length, iteration, step_a):
    for _ in range(iteration):
        triangle = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
        triangle.draw()
        point = triangle.end_point
        angle += step_a


point_0 = sd.get_point(100, 100)
point_1 = sd.get_point(600, 100)
point_2 = sd.get_point(100, 350)
point_3 = sd.get_point(600, 350)


# figures(point=point_0, angle=20, length=100, iteration=3, step_a=120)
# figures(point=point_1, angle=20, length=100, iteration=4, step_a=90)
# figures(point=point_2, angle=20, length=100, iteration=5, step_a=72)
# figures(point=point_3, angle=20, length=100, iteration=6, step_a=60)
sd.pause()
# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!
