import simple_draw as sd

sd.resolution = (1200, 600)

# Нарисовать пузырек - три вложенных окружностей с шагом 5 пикселей
def figure(a, b):
    pooint = sd.get_point(a, b)
    sd.square(left_bottom=pooint, side=100)
    pooint_2 = sd.get_point(a + 25, b + 100)
    sd.square(left_bottom=pooint_2 )
    pooint_3 = sd.get_point(a + 45, b + 150)
    sd.square(left_bottom=pooint_3, side=25)

figure(69 , 320)
figure(600, 20)
sd.pause()