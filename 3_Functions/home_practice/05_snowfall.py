# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 800)
# На основе кода из практической части реализовать снегопад:
# - создать списки данных для отрисовки N снежинок
# - нарисовать падение этих N снежинок
# - создать список рандомных длинн лучей снежинок (от 10 до 100) и пусть все снежинки будут разные

# N = 20.


# Пригодятся функции
# sd.get_point()
# sd.snowflake()
# sd.sleep()


# Примерный алгоритм отрисовки снежинок
#   навсегда
#     очистка экрана
#     для индекс, координата_х из списка координат снежинок
#       получить координата_у по индексу
#       создать точку отрисовки снежинки
#       нарисовать снежинку цветом фона
#       изменить координата_у и запомнить её в списке по индексу
#       создать новую точку отрисовки снежинки
#       нарисовать снежинку на новом месте белым цветом
#     немного поспать
#     если пользователь хочет выйти
#       прервать цикл

# TODO здесь ваш код
x_cor = []
for x in range(50, 51 * 20, 50):
    x_cor.append(x)

y_cor = []
for _ in range(20):
    while True:
        y = sd.random_number(a=700, b=800)
        if y in y_cor:
            y = sd.random_number(a=700, b=800)
        else:
            break
    y_cor.append(y)

length_0 = []
for _ in range(20):
    while True:
        len_a = sd.random_number(a=10, b=30)
        if len_a in length_0:
            len_a = sd.random_number(a=10, b=30)
        else:
            break
    length_0.append(len_a)

# while True:
#     sd.clear_screen()
#     for index, x in enumerate(x_cor):
#         y = y_cor[index]
#         length = length_0[index]
#         snow_point = sd.get_point(x, y)
#         sd.snowflake(center=snow_point, length=length, color=sd.background_color)
#         y_cor[index] -= sd.random_number(a=10, b=50)
#         x_cor[index] -= sd.random_number(a=-50, b=50)
#         snow_point = sd.get_point(x_cor[index], y_cor[index])
#         sd.snowflake(center=snow_point, length=length, color=sd.COLOR_WHITE)
#         if y_cor[index] < 50:
#             y_cor[index] += 700
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# Часть 2 (делается после зачета первой части)
#
# Ускорить отрисовку снегопада
# - убрать clear_screen() из цикла
# - в начале рисования всех снежинок вызвать sd.start_drawing()
# - на старом месте снежинки отрисовать её же, но цветом sd.background_color
# - сдвинуть снежинку
# - отрисовать её цветом sd.COLOR_WHITE на новом месте
# - после отрисовки всех снежинок, перед sleep(), вызвать sd.finish_drawing()

# Усложненное задание (делать по желанию)
# - сделать рандомные отклонения вправо/влево при каждом шаге
# - сделать сугоб внизу экрана - если снежинка долетает до низа, оставлять её там,
#   и добавлять новую снежинку

# TODO здесь ваш код
while True:
    sd.start_drawing()
    for index, x in enumerate(x_cor):
        y = y_cor[index]
        length = length_0[index]
        snow_point = sd.get_point(x, y)
        sd.snowflake(center=snow_point, length=length, color=sd.background_color)
        y_cor[index] -= sd.random_number(a=0, b=50)
        x_cor[index] -= sd.random_number(a=-50, b=50)
        snow_point = sd.get_point(x_cor[index], y_cor[index])
        sd.snowflake(center=snow_point, length=length, color=sd.COLOR_WHITE)
        if y_cor[index] < 50:
            p_point = sd.get_point(x_cor[index], y_cor[index])
            sd.snowflake(center=p_point, length=length, color=sd.COLOR_WHITE)
            y_cor[index] += 700
    sd.finish_drawing()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break
