# -*- coding: utf-8 -*-
from district.central_street.house1.room1 import folks as f_1
from district.central_street.house1.room2 import folks as f_2
from district.central_street.house2.room1 import folks as f_3
from district.central_street.house2.room2 import folks as f_4
from district.soviet_street.house1.room1 import folks as f_5
from district.soviet_street.house1.room2 import folks as f_6
from district.soviet_street.house2.room1 import folks as f_7
from district.soviet_street.house2.room2 import folks as f_8
# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join
f_0 = f_1 + f_2 + f_3 + f_4 + f_5 + f_6 + f_7 + f_8
print('На районе живут:\n', '\n'.join(f_0))
