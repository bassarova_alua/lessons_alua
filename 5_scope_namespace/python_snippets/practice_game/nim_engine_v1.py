import random

bunches = []


def put_stones():
    global bunches
    bunches = []
    for _ in range(3):
        qua = random.randint(1, 20)
        bunches.append(qua)


def get_bunches():
    global bunches
    print(bunches)


def take_from_bunch(pos, qua):
    global bunches
    if pos <= 3:
        if bunches[pos - 1] >= qua:
            bunches[pos - 1] -= qua
            return True
        else:
            return False
    else:
        return False


def game_over():
    global bunches
    return sum(bunches) == 0
